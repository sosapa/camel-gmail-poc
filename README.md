# README #

POC for accessing Gmail account using apache camel. This will be required to access invoice information received by the Invoice Reconciliation team and possible other areas.

### What is this repository for? ###

* Proof of concept of Apache Camel Google Mail
* [GoogleMail component Reference documentation](http://camel.apache.org/googlemail.html)
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
You will need to create an account and generate OAuth credentials. Credentials comprise of a clientId, clientSecret, and a refreshToken.
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact