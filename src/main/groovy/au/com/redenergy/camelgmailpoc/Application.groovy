// Copyright 2018 Red Energy
// Pablo Sosa (psosa_ar@yahoo.com)
// 2018-02-02

package au.com.redenergy.camelgmailpoc

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.web.support.SpringBootServletInitializer

@SpringBootApplication
class Application extends SpringBootServletInitializer {

  static void main(String[] args) {
    SpringApplication.run(Application, args)
  }

  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
    return builder.sources(Application)
  }

}
