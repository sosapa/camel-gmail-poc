// Copyright 2018 Red Energy
// Pablo Sosa (psosa_ar@yahoo.com)
// 2018-02-02

package au.com.redenergy.camelgmailpoc

import org.apache.camel.builder.RouteBuilder
import org.springframework.stereotype.Component

@Component(value = "camelGmailPoc")
class GMailRouteBuilder extends RouteBuilder {

  @Override
  void configure() throws Exception {
    log.info("Starting route config")
    from("google-mail://messages/list?clientId={{gmail.poc.clientId}}&clientSecret={{gmail.poc.clientSecret}}&refreshToken={{gmail.poc.refreshToken}}&userId={{gmail.poc.userId}}}")
        .routeId("listGmailMessagesRouteId")
        .log("Received message [\${body}")

  }
}